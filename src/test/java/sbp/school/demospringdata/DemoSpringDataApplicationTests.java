package sbp.school.demospringdata;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;
import sbp.school.demospringdata.pojo.Developer;
import sbp.school.demospringdata.repositories.DevelopersCrudRepository;

import java.util.List;
import java.util.Objects;

@SpringBootTest
class DemoSpringDataApplicationTests {

    @Autowired
    private DevelopersCrudRepository developersCrudRepository;

    @Test
    void contextLoads() {
        List<Developer> developerList = this.developersCrudRepository.findBySpecialty("JAVA");

        Assert.isTrue(!developerList.isEmpty());
        Assert.isTrue(Objects.nonNull(developerList.get(0)));

        List<Developer> developersFirstNameLike = this.developersCrudRepository.findByFirstNameIsLike("Иван");

        Assert.isTrue(!developersFirstNameLike.isEmpty());
        Assert.isTrue(Objects.nonNull(developersFirstNameLike.get(0)));

        Developer developerSpec = this.developersCrudRepository.getFirstBySpecialty("C++");

        Assert.hasText("Петр", developerSpec.getFirstName());
        Assert.isTrue(Objects.nonNull(developerSpec));

        List<Developer> developerListWithExperience =
                this.developersCrudRepository.findDeveloperWithExperience(3);

        Assert.isTrue(!developerListWithExperience.isEmpty());
        Assert.isTrue(Objects.nonNull(developerListWithExperience.get(0)));

        List<Developer> developerListWithLastName = this.developersCrudRepository.findByLastNameIsLike("Владимиров");

        Assert.isTrue(!developerListWithLastName.isEmpty());
        Assert.isTrue(Objects.nonNull(developerListWithLastName.get(0)));

        List<Developer> developersBySpecialtyOrderByExperienceAsc =
                this.developersCrudRepository.findBySpecialtyOrderByExperienceAsc("JAVA");

        Assert.isTrue(!developersBySpecialtyOrderByExperienceAsc.isEmpty());
        Assert.isTrue(Objects.nonNull(developersBySpecialtyOrderByExperienceAsc.get(0)));
    }
}
