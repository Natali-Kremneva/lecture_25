package sbp.school.demospringdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import sbp.school.demospringdata.pojo.Developer;
import sbp.school.demospringdata.repositories.DeveloperPagingAndSortingCrudRepository;
import sbp.school.demospringdata.repositories.DevelopersCrudRepository;
import sbp.school.demospringdata.repositories.MyCustomDeveloperRepository;

import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class DemoSpringDataApplication implements CommandLineRunner {

    @Autowired
    private DevelopersCrudRepository developersCrudRepository;

    @Autowired
    private DeveloperPagingAndSortingCrudRepository developerPagingAndSortingCrudRepository;

    @Autowired
    private MyCustomDeveloperRepository myCustomDeveloperRepository;


    public static void main(String[] args) {
        SpringApplication.run(DemoSpringDataApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        Developer developer = new Developer("Владимир", "Иванов", "Java", 1);

        this.developersCrudRepository.save(developer);

        List<Developer> developerList = this.developersCrudRepository.findBySpecialty("JAVA");
        System.out.println("Developer list: " + developerList);

        List<Developer> developersFirstNameLike = this.developersCrudRepository.findByFirstNameIsLike("Иван");
        System.out.println("Developers firstNameLike list: " + developersFirstNameLike);

        Developer developerSpec = this.developersCrudRepository.getFirstBySpecialty("C++");
        System.out.println("Developer firstBySpecialty: " + developerSpec);

        List<Developer> developerListWithLastName = this.developersCrudRepository.findByLastNameIsLike("Владимиров");
        System.out.println("Developers ListWithLastName list: " + developerListWithLastName);

        List<Developer> developersBySpecialtyOrderByExperienceAsc =
                this.developersCrudRepository.findBySpecialtyOrderByExperienceAsc("JAVA");
        System.out.println("Developers bySpecialtyOrderByExperienceAsc list: " + developersBySpecialtyOrderByExperienceAsc);

        List<Developer> developerListWithExperience =
                this.developersCrudRepository.findDeveloperWithExperience(3);
        System.out.println("Developer with experience: " + developerListWithExperience);

        Page<Developer> developerPage =
                this.developerPagingAndSortingCrudRepository.findAll(PageRequest.of(1, 2));
        System.out.println("Developer value: " + developerPage.getTotalElements());
        System.out.println("Developer page value: " + developerPage.getTotalPages());
        System.out.println("First Developer page: " + developerPage.get().collect(Collectors.toList()));

        List<Developer> developerListFromCustomRepository =
                this.myCustomDeveloperRepository.findAllDevelopers();
        System.out.println("Developers from custom repository: " + developerListFromCustomRepository);

    }
}
