package sbp.school.demospringdata.repositories;

import org.springframework.stereotype.Repository;
import sbp.school.demospringdata.pojo.Developer;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class MyCustomDeveloperRepositoryImpl implements MyCustomDeveloperRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Developer> findAllDevelopers() {

        List<Developer> developerList = this.entityManager.createQuery("from Developer ").getResultList();
        System.out.println("All developers: " + developerList);
        return developerList;
    }
}
