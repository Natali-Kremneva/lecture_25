package sbp.school.demospringdata.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sbp.school.demospringdata.pojo.Developer;

import java.util.List;

@Repository
public interface DevelopersCrudRepository extends CrudRepository<Developer, Integer> {

    List<Developer> findBySpecialty(String specialty);

    List<Developer> findByFirstNameIsLike(String name);

    Developer getFirstBySpecialty(String specialty);

    @Query("select d from Developer d where d.experience = :exp")
    List<Developer> findDeveloperWithExperience(@Param("exp") int experience);

    @Query("select d from Developer d where d.lastName = :name")
    List<Developer> findByLastNameIsLike(@Param("name") String name);

    @Query("select d from Developer d where d.specialty = :specialty order by d.experience asc")
    List<Developer> findBySpecialtyOrderByExperienceAsc(String specialty);

}
