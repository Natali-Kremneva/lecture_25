package sbp.school.demospringdata.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import sbp.school.demospringdata.pojo.Developer;

public interface DeveloperPagingAndSortingCrudRepository extends PagingAndSortingRepository<Developer, Integer> {

}
