package sbp.school.demospringdata.repositories;

import sbp.school.demospringdata.pojo.Developer;
import java.util.List;

public interface MyCustomDeveloperRepository {
    List<Developer> findAllDevelopers();
}
